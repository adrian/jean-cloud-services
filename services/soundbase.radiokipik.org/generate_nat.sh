function extract_ports_from_compose {
	if [ "$#" -ne 1 ] ; then
		echo "function extract_ports_from_dockerfile needs 1 parameter : docker-compose file" >&2
		exit 1
	fi
    ports=false
    while read line ; do
    	if [ "$line" = 'ports:' ] ; then
    		ports=true
    	elif "$ports" ; then
    		if [[ "$line" != -* ]] ; then
    			ports=false
    		else
    			echo $line | tr -d ' ' | tail -c +2
    		fi
    	fi
    done < docker-compose.yml
}

extract_ports_from_compose docker-compose.yml | ../_deployer/template.sh .env
