#!/bin/bash
# Ce script sert à lister les IPs du répertoire courant (donc des services docker) pour savoir quels réseaux sont encore disponibles.
# On part du principe que chaque service a un réseau /24 dédié
grep -ho '172.29.[^.]\+' . -r | sort -u
