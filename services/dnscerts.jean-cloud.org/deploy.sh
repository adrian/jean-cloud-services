#!/bin/bash

set -euo pipefail

# For some variables
. /etc/jeancloud.env

apt install -y python3-certbot-dns-rfc2136
