#!/bin/bash

if [ "$#" -ne 2 ] ; then
	echo "Usage: $0 <service_name> <workdir>" >&2
	exit 1
fi

service="$1"
workdir="$2"

nginxfile="/docker/$service/nginx_server.conf"
if [ -f "$nginxfile" ] ; then    
	nginxdomains="$(extract_domain_nginx_conf.sh "$nginxfile" | template.sh "/docker/$service/.env")"
	domains="$(echo "$nginxdomains" | tr ' ' '\n' | sort -u | resolvable.sh ns.jean-cloud.org | sed -z -e 's/\n$//' -e 's/\n/ -d /g' )"
	[ -z "$domains" ] && exit 0
	echo "--------------- -d $domains"
	certbot certonly --config-dir "$DATA_DIR/certs" --work-dir "$workdir/work" --logs-dir "$workdir/logs" --agree-tos -m contact@jean-cloud.org -n --cert-name "$service" --dns-rfc2136 --dns-rfc2136-credentials "$DATA_DIR/rfc2136.ini" -d $domains
fi
