#!/bin/bash

. /bin/driglibash-base


if [ "$1" = "uninstall" ] ; then
    echo "Uninstalling"

else # Installation procedure below
    echo "Installing"
    if [ -L /data/soundbase.oma-radio.fr/data/radiodemo ] ; then
        run ln -s /data/soundbase.oma-radio.fr/data/radiodemo /data/radiodemo.oma-radio.fr/soundbase
    fi
fi



