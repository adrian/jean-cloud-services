#!/bin/bash

#if [ -d "$DATA_DIR/core" ] ; then
#	git_update.sh -b dev -i "$DATA_DIR/gitlab-deploy.sshprivkey" -d "$DATA_DIR/core" git@gitlab.com:omaradio/core.git
#fi

chmod 700 "$SECRET_DIR/gitlab-deploy.sshprivkey"
mkdir -p "$HTTP_DIR/manager"
git_update.sh -b 'v3' -i "$SECRET_DIR/gitlab-deploy.sshprivkey" -d "$HTTP_DIR/manager" git@gitlab.com:omaradio/single-manager.git
cd "$HTTP_DIR/manager"
npm install
#npm audit fix
npm run build


#semver_prefix="3."
## Get available versions
#json="$(curl 'https://gitlab.com/api/v4/projects/omaradio%2Fsingle-manager/packages?order_by=version&sort=desc&package_name=manager&status=default' )"
#
## Get the latest one
#latest="$(echo "$json" | grep -Po '"version":"\K'"$semver_prefix"'[^"]+"' | tr -d '"' | sort -g | tail -n 1)"
#if [ -z "$latest" ] ; then
#	echo "Version is empty"
#	exit 1
#fi
#
## Get associated link
#link="$(echo "$json" | jq ".[] | select(.version? | match(\"$latest\")) | ._links.web_path" | sed -e 's/^"//' -e 's/"$//')"
#if [ -z "$link" ] ; then
#	echo "Link is empty"
#	exit 1
#fi
#
#cd "$HTTP_DIR"

#wget https://gitlab.com/omaradio/single-manager/-/package_files/117195410/download -O - | tar xz 
