from alpine:edge

RUN apk update && \
    apk upgrade && \
    apk --no-cache add hugo tzdata && \
    rm -rf /var/cache/apk/*

WORKDIR /srv

ENTRYPOINT ["hugo", "--ignoreCache"]
