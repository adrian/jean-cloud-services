#!/bin/bash
. driglibash-base
. /etc/jeancloud.env

driglibash_section_prefix="[Restart nginx] "

section "Test if nginx conf is ok"
run nginx -t -c "$proxy_dir/new_nginx.conf"

section "Update nginx conf"
run rm -rf "$nginx_conf_path"
run cp -r "$new_nginx_conf_path" "$nginx_conf_path"
run cp "/docker/_proxy/nginx.conf" "$proxy_dir/nginx.conf"

section "Test nginx conf to be sure"
run nginx -t

if [ -z "$(cat /var/run/nginx.pid)" ] ; then
    section "Start nginx"
    run nginx
else
    section "Reload nginx"
    run nginx -s reload
fi
