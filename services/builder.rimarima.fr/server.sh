#!/bin/bash

echo "Content-type: text/html"
echo ""

. .env

action="$(echo "$DOCUMENT_URI" | tr -d '/\;!&<>?#[]()"*')"

echo '<pre>'
deploy_as "$JC_SERVICE"
echo '</pre>'
