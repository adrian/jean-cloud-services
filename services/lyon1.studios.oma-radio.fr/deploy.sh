#!/bin/bash

wgif=omaLyon1
ip=10.100.100.254
run="ip netns exec $wgif"

# Create netns if needed
if ! ip netns | grep -q "$wgif" ; then
    ip netns add "$wgif"
fi

# Create iface
if ! $run ip link | grep -q "$wgif" ; then
    ip link add "$wgif" type wireguard
    ip link set "$wgif" netns "$wgif"
fi

# Set ip
if ! $run ip -4 -o a | grep -q "$ip" ; then
	$run ip a add "$ip" dev "$wgif"
fi

# Set route
if ! $run ip -4 -o r | grep -q "default dev $wgif" ; then
	$run ip r add default dev "$wgif"
fi

# Up iface
$run ip link set up dev "$wgif"

# Load config
$run wg setconf "$wgif" "/etc/wireguard/$wgif.conf"
