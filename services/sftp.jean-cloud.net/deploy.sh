#!/bin/bash

cd "$DATA_DIR"

[ ! -f users.conf ] && touch users.conf

# Create key if not exists
if [ ! -f ssh_host_ed25519_key ] ; then
	ssh-keygen -t ed25519 -f ssh_host_ed25519_key -N ''
	ssh-keygen -t rsa -b 4096 -f ssh_host_rsa_key -N ''
fi
