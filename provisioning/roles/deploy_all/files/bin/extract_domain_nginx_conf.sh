#!/bin/bash

. driglibash-base

if [ "$#" -lt 1 ] ; then
	die "Usage: $0 <nginx_conf_file>"
fi

file="$1"

grep '^[[:blank:]]*[^#][[:blank:]]*server_name' "$file" | sed 's/ _ / /g' | sed   's/server_name//g' | sed 's/default_server//g' | sed -e 's/^[[:space:]]*//' -e 's/;$//'  | sort -u
