#!/bin/bash

set -euo pipefail

. .env

wgif="$1"

echo "
[Interface]
PrivateKey = $(cat $DATA_DIR/privatekey)
Address = 10.29.0.254/32
ListenPort = 55820

# packet forwarding
PreUp = sysctl -w net.ipv4.ip_forward=1

# port forwarding
#PreUp = iptables -t nat -A PREROUTING -p tcp --dport $MUX_SERVER_PORT -j DNAT --to-destination $ENDPOINT:$MUX_SERVER_PORT
#PreUp = iptables -t nat -A PREROUTING -p tcp --dport $TELECOM_SERVER_PORT -j DNAT --to-destination $ENDPOINT:$TELECOM_SERVER_PORT

#PostDown = iptables -t nat -D PREROUTING -p tcp --dport $MUX_SERVER_PORT -j DNAT --to-destination $ENDPOINT:$MUX_SERVER_PORT
#PostDown = iptables -t nat -D PREROUTING -p tcp --dport $TELECOM_SERVER_PORT -j DNAT --to-destination $ENDPOINT:$TELECOM_SERVER_PORT

# packet masquerading
#PreUp = iptables -t nat -A POSTROUTING -o $wgif -j MASQUERADE
#PostDown = iptables -t nat -D POSTROUTING -o $wgif -j MASQUERADE

# remote settings for the private server
[Peer]
PublicKey = 6/Mlxe9auEw/WQnC6QYNAYtSAo8jAEMhJ1wXaRNy4AE=
AllowedIPs = 10.29.0.0/24
"
