#!/bin/bash

# TODO js (dnssec is trusting google right now)
# fetch('https://dns.google/resolve?name=deployer.jean-cloud.org&cd=true&type=a').then(r => {r.json().then(j => {for (const i in j.Answer) {console.log(j.Answer[i].data)}})})


echo "Content-type: text/html"
echo ""

service="$(echo "$DOCUMENT_URI" | tr -d '/\;!&<>?#[]()"*')"
deployer="/docker/$service/deploy_http.sh"
. /etc/jeancloud.env

if [ -z "$service" ] || [ ! -x "$deployer" ] ; then
	echo "error"
else
	set -a
	. "/docker/$service/.env"
	set +a
	"$deployer" 2>&1
	ret="$?"
	if [ "$ret" -ne 0 ] ; then
		echo 'Error'
	else
fi

