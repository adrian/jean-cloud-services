#!/bin/bash

. driglibash-base
. "$(where)/helper_functions.sh"

set -euo pipefail

# Working variables
debian_bind_confdir="/etc/bind"
autoconf_separator=";;; Autogeneration. Do not write under this line! ;;;"


# File that contain "service target" lines
# With service a symbolic dns name and target an existing server hostname
servicefile="/docker/services.txt"

# The bind file containing server declarations
server_zone_file="template.db.jean-cloud.org"

# Where you want your DNS keys stored
keydir="$DATA_DIR/keys"

# IP of primary servers
# MUST end with ; if non-empty
primary_ips=""

# IP of secondary servers (for zone transfer)
# master.retzo.net
secondary_ips="159.69.124.127;2a01:4f8:c17:d8f2::1;"
# tetede
secondary_ips="${secondary_ips}51.195.40.128;"

# NS name
default_dns_name="ns.jean-cloud.org."

CAA_RR='CAA 0 issue "letsencrypt.org;validationmethods=dns-01"'

runthis () {
	if [ "$#" -ne 1 ] ; then
		die "Usage: runthis <primary|secondary>"
	fi

	prepare
	primary_ips="$primary_ips$(fakeresolve_ip_list izzo)"
	secondary_ips="$secondary_ips$(fakeresolve_ip_list ns)"

	line_in_file "primary_ips=\"$primary_ips\"" "$DOCKER_DIR/.env"
	line_in_file "secondary_ips=\"$secondary_ips\"" "$DOCKER_DIR/.env"

	if [ "$1" = "primary" ] ; then
		create_primary_files
	else
		create_secondary_files
	fi

	restart
}

main () {
	aa-disable /etc/apparmor.d/usr.sbin.named
	runthis primary
}

# Do not execute main if script is sourced
! (return 0 2>/dev/null) && main "$@" || true # return 0 whatever happends
