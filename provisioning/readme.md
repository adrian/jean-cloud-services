# Provisioning

## Installer les dépendances pour les playbooks
Il faut avoir installé ansible-playbook et ansible-galaxy. Puis faire ./install.sh
-> Ce fichier contient trop de trucs, il faudrait le mettre à jour…

## Configurer un serveur
Vérifier que le serveur est bien décommenté dans `inventory.ini`
```
ansible-playbook -i inventory.ini playbook.yml
```

## Déployer des services sur un serveur
Envoyer les fichiers de conf sur le serveur.
```
ansible-playbook -i inventory.ini services.yml
```
Déployer les services : (ssh sur le serveur)
```
/docker/_deployer/main.sh
```

## Déployer des services (ancienne méthode)
Cette méthode ne fonctionne pas avec le nouveau script de déploiement.
```
ansible-playbook -i inventory.ini services_vandamme.yml
```

