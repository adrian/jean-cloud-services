#/bin/bash

# Read domains form stdin and echo the ones resolved successfully

server=""
if [ "$#" -ge 1 ] && [ -n "$1" ] ; then
	server="$1"
fi	

while read domain; do
	host "$domain" $server &>/dev/null
	[ "$?" -eq 0 ] && echo "$domain"
done
exit 0
