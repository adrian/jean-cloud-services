#!/bin/bash
set -euo pipefail

# Update git repo
git_update.sh -d "$HTTP_DIR" -b "${GIT_SOURCE_BRANCH:-main}" "$GIT_SOURCE_REPO"

cd "$HTTP_DIR"

# Get remote content files
rclone_ncloud_publiclink.sh

# Invalid cache
#rm -rf "/tmp/hugo_cache_$USER"

cd themes/blist
npm install
cd ../..
npm install postcss-cli

# Build website
HUGO_CACHEDIR="/tmp/hugo_cache_$USER" hugo
