#!/bin/bash

if [ ! -e "$DATA_DIR/.env" ] ; then
	cat > "$DATA_DIR/.env" <<EOF
ICECAST_SOURCE_PASSWORD=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 130)
ICECAST_ADMIN_PASSWORD=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 130) 
ICECAST_RELAY_PASSWORD=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 130) 
LIQUIDSOAP_SOURCE_PASSWORD=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 130)
EOF
fi
