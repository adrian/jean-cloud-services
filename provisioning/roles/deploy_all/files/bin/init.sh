#!/bin/bash

set -euo pipefail

. /etc/jeancloud.env

while IFS=';' read -r id username service server
do
	home="/data/$service"
	uid="$(( $services_uid_start + $id ))"
	if [ -z "$(grep "^$username:" /etc/passwd)" ] ; then
		useradd -M -U -u $uid -d "$home" "$username"
	fi
done < <(grep -v '^#' /docker/services.csv)

apt install -y make gcc

cd /usr/local/bin
make
chown root:root deploy_as
chmod u+s deploy_as
