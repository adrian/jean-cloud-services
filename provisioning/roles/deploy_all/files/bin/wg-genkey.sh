#!/bin/bash

if [ "$#" -ne 1 ] ; then
	echo "Usage: $0 <filename>"
	exit 1
fi

keyfile="$1"    

if [ ! -f "$keyfile" ] ; then    
    touch "$keyfile"    
    chmod 700 "$keyfile"    
    if [ -n "$(lsof "$keyfile")" ] ; then    
        echo "Error, key $keyfile is red"    
        exit 1    
    fi    
    wg genkey > "$keyfile"    
fi    
