#!/bin/bash

set -euo pipefail

. /etc/jeancloud.env

[ ! -f "$DATA_DIR/rfc2136.ini" ] && echo "$0 Missing files" && exit 1

while read line ; do
	read -r service target < <(echo "$line")
	echo "---- $service $target ----"
	nginxfile="/docker/$service/nginx_server.conf"
	if [ -f "$nginxfile" ] ; then    
		nginxdomains="$(extract_domain_nginx_conf.sh "$nginxfile" | template.sh "/docker/$service/.env")"
		domains="$(echo "$service $nginxdomains" | tr ' ' '\n' | sort -u | sed -z 's/\n/ -d /')"
		echo "$domains"
		certbot certonly -m contact@jean-cloud.org -n --cert-name "$service" --dns-rfc2136 --dns-rfc2136-credentials "$DATA_DIR/rfc2136.ini" -d $domains
    fi

done < "$servicefile"
