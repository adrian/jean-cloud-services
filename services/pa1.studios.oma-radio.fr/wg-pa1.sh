#!/bin/bash

set -euo pipefail

. .env
filename="$(basename "$0")"
ifname="${filename:3:-3}"

echo "
[Interface]
PrivateKey = $(cat $DATA_DIR/privatekey)
ListenPort = 51820
Address = 10.100.1.254/32

[Peer] # adrian
PublicKey = 14yKNmSfD2lrWU+d/RJBPNvh9pZ/nW4bK27F9nTgvk0=
AllowedIPs = 10.100.1.253/32

[Peer] # Nico
PublicKey = jsXBs8tZn1sWT73xx3DWEdGAWv6SjfQ2TAxX+8pL6mU=
AllowedIPs = 10.100.1.252/32

[Peer] # Passerelle
PublicKey = unY6v95qus8ttJvmSlxqa+J8lKj+CCiRItZ3pFwyjyM=
AllowedIPs = 10.100.1.0/24,192.168.100.0/24

[Peer] # debug
PublicKey = K9IpoUbjyN+42y0YG3OIwAPRBZcd92GnKfbYEj3RZ18=
AllowedIPs = 10.100.1.21/32
"
