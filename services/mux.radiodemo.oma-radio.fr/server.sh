#!/bin/bash

echo "Content-type: text/html"
echo ""

. .env


instance=''
since=''
until=''

action="$(echo "$QUERY_STRING" | tr -d '/\;!<>?#[]()"*.' | sed 's/&/\n/g')"

while IFS='=' read key value ; do
	case "$key" in
		instance)
			instance="$value"
			;;
		since)
			since="$value"
			;;
		until)
			until="$value"
			;;
		list)
			docker-compose ps -a --format json
			;;
		*)
			exit 1
	esac
done < <(echo "$action")

[ -z "$instance" ] && exit 2
[ -z "$since" ] && exit 3
[ -z "$until" ] && exit 4

echo docker-compose logs --since "$since" --until "$until" "$instance"
if [ "$?" -ne 0 ] ; then
	echo failed
fi

