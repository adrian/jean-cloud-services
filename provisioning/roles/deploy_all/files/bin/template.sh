#!/bin/bash
if [ "$1" = '-h' ] || [ "$1" = '--help' ] ; then
	echo "Usage: $0 <env_file>" >&2
	echo "This script read env_file variables and replace theire occurences in stdin" >&2
	exit 0
fi
if [ -f "$1" ] ; then
	bash -c 'set -a && . '"$1"' &&  envsubst "$(cat '"$1"' | grep -o ^.*= | sed "s/=//" | sed "s/^/$/")"'
else
	echo "No env file found, no modifications made." >&2
	cat /dev/stdin
fi
