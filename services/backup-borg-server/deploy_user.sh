#!/bin/bash

sshkey=~/.ssh/borg-server
backup_dir="$DATA_DIR/backups"

mkdir -p ~/.ssh "$backup_dir"

if [ ! -e "$sshkey" ] ; then
	ssh-keygen -q -C 'Borg server ssh key' -N '' -t rsa  -f "$sshkey" <<<y 2>&1 >/dev/null
fi

echo -n "" > ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys

# Foreach client
for client in raku.jean-cloud.org izzo.jean-cloud.org ; do
	# Generate key
	clientkey="$(mktemp -d)"
	ssh-keygen -q -N '' -t rsa -C 'Borg client ssh key' -f "$clientkey/id_rsa" <<<y 2>&1 >/dev/null
	cat > ~/.ssh/authorized_keys <<EOF
command="borg serve --append-only --restrict-to-path '$backup_dir/$client'",restrict $(cat "$clientkey/id_rsa.pub")
EOF
	
	# Create needed directory
	mkdir -p "$backup_dir/$client"

	# Trigger the backup
	eval $(ssh-agent) > /dev/null
	ssh-add "$clientkey/id_rsa"
	ssh -A -R localhost:12345:127.0.0.1:45985 "root@$client" -p 45985 -i "$sshkey"

	# Clean
	kill "${SSH_AGENT_PID}"
	rm -r "$clientkey" ~/.ssh/authorized_keys
done




