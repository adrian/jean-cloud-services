#!/bin/bash

driglibash_run_retry=true
. driglibash-base
set -u

# Already done at upload time ?	
#run gen_env.sh

###############################################################################
#                       Variables
###############################################################################

set -a
. /etc/jeancloud.env
set +a

###############################################################################
#                       Helpers
###############################################################################

# Path to this directory
here="$(where 'follow_links')"


###############################################################################
#                       Test system requirements
###############################################################################

[ ! -f /data/mounted ] && die "/data is not mounted"

###############################################################################
#                       Nginx preparation
###############################################################################

driglibash_section_prefix="[Prepare nginx] "
section "Delete new conf directory (to start from scratch)"
run rm -rf "$new_nginx_conf_path"

section "Create new conf file (for tests purposes)"
sed "s#$nginx_conf_path#$new_nginx_conf_path/#" "/docker/_proxy/nginx.conf" > "$proxy_dir/new_nginx.conf"

section "Create proxy dir"
run mkdir -p "$proxy_dir" /docker /data
run chown root:root /docker
run chown root:root /data
run chmod 755 /docker
run chmod 755 /data

section "Create new conf directory"
run mkdir -p "$new_nginx_conf_path"

###############################################################################
#                       Deploy services
###############################################################################

section "Start docker"
run systemctl start docker docker.socket

# List of failed services
failed=""

while IFS=';' read -r id username service target
do

	echo -n "$service -> "
    [ ! -d "/docker/$service" ] && die "/docker/$service directory not found"

	# Check if service target is localhost
	[[ "$(getent hosts $target)" != "::1 "* ]] && echo 'Not here' && continue
	
	echo "Deploying"
    deploy_service.sh "$service" "noreload"

	if [ "$?" -ne 0 ] ; then
		failed="$failed $service"
	fi

done < <(grep -v '^#' /docker/services.csv)    

restart_nginx.sh

if [ -n "$failed" ] ; then
	echo "FAILED SERVICES"
	echo "$failed"
fi

clean
