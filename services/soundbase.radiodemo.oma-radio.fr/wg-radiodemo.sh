#!/bin/bash

set -euo pipefail

. .env

[ -f "$DATA_DIR/radiodemo-soundbase.wgkey" ] || { echo 'No privatekey found' >&2 && exit 1 ; }

echo "
[Interface]
PrivateKey = $(cat "$DATA_DIR/radiodemo-soundbase.wgkey")
Address = 10.29.0.1/32
ListenPort = 55820

[Peer]
PublicKey = iwIsUriF4CT/Jpu29VXlj43hT3bUjG67FeEgCTcQCVc=
AllowedIPs = 10.29.0.254/32
Endpoint = mux.radiodemo.oma-radio.fr:55820
PersistentKeepalive = 30
"
