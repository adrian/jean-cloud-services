#!/bin/bash
set -euo pipefail

# Update git repo
git_update.sh -d "$HTTP_DIR" "$GIT_SOURCE_REPO"

cd "$HTTP_DIR"

rclone_ncloud_publiclink.sh

# Create and source venv
if [ ! -e venv ] ; then
	virtualenv venv
fi
. venv/bin/activate

# Install dependencies
pip3 install -r requirements.txt

# Build website
make html

# Old way of doing python stuff
#podman run -v "$HTTP_DIR:/usr/local/app" docker.io/jeancloud/pelican
