#!/bin/bash

. driglibash-base

if [ "$#" -ne 2 ] ; then
    echo "usage: $0 <action> <service>"
    echo "action is start/stop/reload/restart"
    echo "service is a jc service name"
    exit 1
fi
action="$1"
service="$2"
if [ -f "/docker/$service/install.sh" ] ; then
    section "Running install script"
    . "/docker/$service/install.sh"
    # Is $action a bash function?
    if [ -n "$(LC_ALL=C type "$action" | head -n 1 | grep 'function')" ] ; then
        (source "/docker/$service/.env" && "$action")
	else
		die "$0 no action $action found for service $service"
    fi
fi
