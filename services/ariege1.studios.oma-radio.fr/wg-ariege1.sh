#!/bin/bash

set -euo pipefail

. .env
filename="$(basename "$0")"
ifname="${filename:3:-3}"

echo "
[Interface]
PrivateKey = $(cat $DATA_DIR/privatekey)
ListenPort = 51822
Address = 10.100.2.254/32

[Peer] # adrian
PublicKey = 34DD9W9Pr2EpVK4IvU3tVY6fsIvGqDisUYr5Xtk62FI=
AllowedIPs = 10.100.2.253/32

[Peer] # Passerelle
PublicKey = SM40+PyJSNk+Rmsa7Ym4+PwBgkRlRCsqEC7s7wfo/QE=
AllowedIPs = 10.100.2.0/24,192.168.100.0/24
"
