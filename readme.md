# Services Jean-Cloud


## Dossier installing
Contient des scripts sh pour installer debian sur un disque dur. Qui ira ensuite se brancher dans un ordi :)

## Dossier provisioning
Contient
- des rôles ansible pour configurer les serveurs
- un rôle ansible pour envoyer les services sur les serveurs (rsync bête)
- des scripts maisons à envoyer sur les serveurs

## Dossier services
Les services à faire tourner.

Chaque service qui tourne sur jean-cloud est composé :
- Un uid et username pour des questions de droits
- `deploy.sh` d’un script d’installation
- `docker-compose.yaml` d’un fichier docker-compose
- `nginx_server.conf` d’un fichier de conf nginx
- `deploy_http.sh` d’un script de déploiement web (qui est exécuté avec l’utilisateur www-data et peut être exécuté par le serveur web lui-même)
- `wg-*.sh` Script qui génère une config wireguard pour l’interface *

Chaque élément est facultatif.

Chaque élément est éxécuté, démarré ou installé dans l’ordre par `deploy_service.sh`



## scripts
dans `provisioning/roles/deploy_all/files/bin`

-`deployall.sh` va pour chaque service vérifier s’il doit tourner sur la machine actuelle et lance le `deploy_service.sh` si c’est le cas.
- `letsencrypt.sh` va renouveler tous les certificats dont nginx a besoin (il va lire dans /etc/nginx/sites-enabled).
- `git_update.sh` récupère une copie à jour d’un dépôt git (fait un clone ou pull en fonction des besoins) et s’assure de ne pas garder tout l’historique du dépôt.


## Variables
Les scripts ont accès aux variables suivantes :
- `DATA_DIR` : là où sauvegarder des données.
- `DOCKER_DIR` : dossier contenant les fichiers de déploiement du service.
- `HTTP_DIR` : là où mettre les fichiers web si ils sont statiques. Ce dossier peut être détruit à tout moment, il n’est pas sauvegardé.
- `JC_SERVICE` : le nom du dossier service. Correspond souvent à l’adresse du service.
Ces variables sont ajoutées au ficher .env du service par le script `gen_env.sh`.

