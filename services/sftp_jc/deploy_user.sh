#!/bin/bash

mkdir -p ~/.ssh
echo '[sftp.jean-cloud.net]:2929 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC5WLG4HbhHMWZySQkCOfMyJ8HAojyJJ66prhn/WSh1T6vyi/oWuodhN5fzIenEpKrmIzXLLfFa8Q9YuEYmcwNGp0FrTnATDYLABL530DBsCQzA5S+fLecY0iLHEkluuJCcX2+cNpu7ytzgbEzHFDRDkd6RgI1cBL5smCnbxfrJSvfLR0VvihQJNZKPYEFHxt9euGkHhKilwbXa4VEtfY8GfMK18dGlM+K2nRu4G+ckoNzx9K2RdwAqUyj1WFLrS+FYY8EbHlffWf4WES32tSeyd6z3rF0jFoN83ojZ0nPfcch10tWklfRc2RcoItde9p0bR/Cgz2SI1nZT9EcBhrpqGkucLsx/xvcxXd/puQDe9svBq1vgWRuub7e6Q40v8Lhl5dTGJutKLWbCg9udonaUIR/LudSnSgEGrDoz/Yjx8Iytj9+tLj9C+rRASfUnHnTRjsVKJG3Ofx+YHhWs0gLJcV06EvM+PBgZOCnegcPrssi0OE+I5HtDixTkaBNQPHXfKcGqlSEPRmqEuwsvdcv7vOfWJn1ufmKpoaLXWPrX+U4gZYSMHrhieqdcieT8wSu/YJa8gBM5qkK1WMOYxwYzLU5q9H4g9sJfFqk14cvQk47Tb+nTLlFoYZvunATBAxQuNuMo8+scu0BuIqinB5SjO6bwF3nJUCigs8BTmo95Pw==' > ~/.ssh/known_hosts
chmod 700 ~/.ssh/known_hosts

rclone sync --config=/notfound --sftp-host sftp.jean-cloud.net --sftp-user "$SFTP_USER" --sftp-port 2929 --sftp-key-file "$SECRET_DIR/sftp.sshprivkey" --sftp-known-hosts-file ~/.ssh/known_hosts :sftp:/public/ "$HTTP_DIR"
