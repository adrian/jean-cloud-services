#!/bin/bash

set -euo pipefail

. .env

wgif="$1"

echo "
[Interface]
PrivateKey = $(cat $DATA_DIR/$wgif.wgprivatekey)
ListenPort = $((51800+$JC_ID))
#Address = 10.100.100.254/32

[Peer] # Adrian
PublicKey = p4/km7Rtl5IgYGw8OPIyE0/f8UoRbcMJwkVJ0Zyv/C8=
AllowedIPs = 10.100.100.253/32

[Peer] # Nico
PublicKey = jsXBs8tZn1sWT73xx3DWEdGAWv6SjfQ2TAxX+8pL6mU=
AllowedIPs = 10.100.100.252/32

[Peer] # Passerelle
PublicKey = ZTKOW5DE8jPO8oMh5hAw/c1MQSlUaVxInMPz9Zdwzwo=
AllowedIPs = 10.100.100.0/24,192.168.100.0/24

[Peer] # Debug
PublicKey = K9IpoUbjyN+42y0YG3OIwAPRBZcd92GnKfbYEj3RZ18=
AllowedIPs = 10.100.100.21/32
"
