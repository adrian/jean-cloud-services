#!/bin/bash
set -euo pipefail

git_update.sh -d "$HTTP_DIR" -b 2020 "https://git.jean-cloud.net/adrian/feteducourt-static.git"
