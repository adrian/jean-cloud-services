#!/bin/bash

. /bin/driglibash-base

fstab_content='storage@195.154.226.199:/data   /data/soundbase.oma-radio.fr/data/      fuse.sshfs      defaults,_netdev,allow_other,default_permissions,reconnect,IdentityFile=/data/soundbase.oma-radio.fr/Niilos     0'

if [ "$1" = "uninstall" ] ; then
    echo "Uninstalling mountpoint"
    umount /data/soundbase.oma-radio.fr/data
    run sed -i "s#$fstab_content##" /etc/fstab
    run sed -i '/195.154.226.199/d' ~/.ssh/known_hosts

else # Installation procedure below
    echo "Installing mountpoint"
    run mkdir -p /data/soundbase.oma-radio.fr/data/
    run line_in_file "$fstab_content" /etc/fstab
    grep 'storage@195.154.226.199:/data' /etc/fstab
    if [ "$?" -ne 0 ] ; then
        run mount /data/soundbase.oma-radio.fr/data
    fi
    
    grep 195.154.226.199 ~/.ssh/known_hosts &> /dev/null
    if [ "$?" -ne 0 ] ; then
        run ssh-keyscan 195.154.226.199 >> ~/.ssh/known_hosts 2> /dev/null
    fi
fi



