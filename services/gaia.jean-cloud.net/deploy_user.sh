#!/bin/bash
set -euo pipefail

git_update.sh -d "$HTTP_DIR" -b "${GIT_SOURCE_BRANCH:-main}" "$GIT_SOURCE_REPO"

