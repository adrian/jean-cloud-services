#!/bin/bash
format="$(grep -oP "^[[:space:]]*log_format main '\K[^;]*(?=';$)" nginx.conf | sed 's/"/\\\\"/g')"
echo "$format"
sed -e "s/%HOST%/$(hostname)/g" -e "s/%FORMAT%/$format/g" exporter_config_template.yml > exporter_config.yml
