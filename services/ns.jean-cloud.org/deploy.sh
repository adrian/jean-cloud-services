#!/bin/bash

set -euo pipefail

cd ../ns1.jean-cloud.org
set -a
. .env
. deploy.sh
set +a

# Do not run if primary exists
[ -d "$DATA_DIR/keys" ] && echo 'ns1 found on this host. Aborting.' && exit 1

export keydir=""
runthis secondary
