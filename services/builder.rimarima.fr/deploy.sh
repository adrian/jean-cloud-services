#!/bin/bash

chown $UID:www-data "$SECRET_DIR/user.htpasswd"
chmod 740 "$SECRET_DIR/user.htpasswd"
