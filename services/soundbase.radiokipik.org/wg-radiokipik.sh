#!/bin/bash

set -euo pipefail

. .env

[ -f "$DATA_DIR/soundbase.wgkey" ] || { echo 'No privatekey found' >&2 && exit 1 ; }

echo "
[Interface]
PrivateKey = $(cat "$DATA_DIR/soundbase.wgkey")
Address = 10.29.60.1/32
ListenPort = 55860

[Peer]
PublicKey = 3ADrLVxzVqLHV530cT+paM+zNQBvm3KCW0voIN1wVBQ=
AllowedIPs = 10.29.60.254/32
Endpoint = mux.radiokipik.org:55825
PersistentKeepalive = 30
"
