#!/bin/bash
set -euo pipefail

if [ "$#" -ne 1 ] || [ -z "$1" ] ; then
	echo "Usage: $0 <service_name>"
	exit 1
fi

service="$1"

user_file="/docker/$service/deploy_user.sh"

if [ ! -f "$user_file" ] ; then
	echo "No such file: $user_file"
	exit 1
fi

unset XDG_RUNTIME_DIR DBUS_SESSION_BUS_ADDRESS

# Source env_files
env_files="/docker/$service/.env /data/secrets/$service/.env /data/$service/.env"
set -a
export HOME="/data/$service/"
for file in $env_files ; do
	if [ -f "$file" ] ; then
		. "$file"
	fi
done
set +a

# Source/exec the user deploy file
cd "/docker/$service"
. "$user_file"
